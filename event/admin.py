import reversion

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.admin.views.main import ChangeList

from mptt.admin import MPTTModelAdmin
from multilingual_model.admin import TranslationInline

from redactor.widgets import RedactorEditor

from cmsbase.admin import PageAdmin, PageFormAdmin, PublishingWorkflowAdmin
from cmsbase.widgets import AdminImageWidget, AdminCustomFileWidget

from event.models import *


# Event translation

class EventTranslationInlineFormAdmin(forms.ModelForm):
    slug = forms.SlugField(label=_('Event URL'))
    content = forms.CharField(widget=RedactorEditor(redactor_css="/static/css/redactor-editor.css"), required=False)

    class Meta:
        model = EventTranslation

    def has_changed(self):
        """ Should returns True if data differs from initial.
        By always returning true even unchanged inlines will get validated and saved."""
        return True

class EventTranslationInline(TranslationInline):
    model = EventTranslation
    form = EventTranslationInlineFormAdmin
    extra = 0 if settings.PREFIX_DEFAULT_LOCALE else 1
    prepopulated_fields = {'slug': ('title',)}
    template = 'admin/cmsbase/cms_translation_inline.html'


# Event feature images

# class ImageInlineForm(forms.ModelForm):
#   image = forms.ImageField(label=_('Image'), widget=AdminImageWidget)
#   class Meta:
#       model=EventImage

# class EventImageInline(admin.TabularInline):
#   form = ImageInlineForm
#   model = EventImage
#   extra = 0
#   template = 'admin/cmsbase/page/images-inline.html'

# Attendees
class EventAttendeeInline(admin.TabularInline):
    model = EventAttendee
    extra = 0
    template = 'admin/cmsbase/page/images-inline.html'

# Speakers
class EventSpeakerInline(admin.TabularInline):
    model = EventSpeakerRelation
    extra = 0
    template = 'admin/cmsbase/page/images-inline.html'

# Links
class EventLinkInline(admin.TabularInline):
    model = EventLink
    extra = 0
    template = 'admin/cmsbase/page/images-inline.html'

# Event

class EventAdminForm(PageFormAdmin):
    from filemanager.widgets import MultipleFileWidget
    categories = forms.ModelMultipleChoiceField(queryset=Category.objects.filter(), widget=forms.CheckboxSelectMultiple, required=False)
    images = forms.CharField(widget=MultipleFileWidget, required=False)
    class Meta:
        model = Event

class EventAdmin(reversion.VersionAdmin, PublishingWorkflowAdmin):
    form = EventAdminForm
    
    inlines = (EventTranslationInline, EventSpeakerInline, EventAttendeeInline, EventLinkInline) # EventImageInline,
    ordering = ['-start_date'] 

    # Override the list display from PublishingWorkflowAdmin
    def get_list_display(self, request, obj=None):
        if not settings.PREFIX_DEFAULT_LOCALE:
            return ['title', 'is_published', 'approval', 'start_date', 'end_date', 'template', 'download_rsvp']
        else:
            return ['title', 'is_published', 'approval', 'start_date', 'end_date', 'template', 'languages', 'download_rsvp']

    def download_rsvp(self, obj):
        return '<a href="%s">%s</a>' % (reverse('event:event-export-rsvp',kwargs={'object_id':obj.id}), 'Download CSV')
    download_rsvp.short_description = 'RSVP' 
    download_rsvp.allow_tags = True

    fieldsets = (
        ('Settings', {
            #'description':_('The page template'),
            'classes': ('default',),
            'fields': ('template', 'start_date', 'end_date', 'location', 'slug',)
        }),
        ('RSVP', {
            #'description':_('The page template'),
            'classes': ('default',),
            'fields': ('capacity',)
        }),
        ('Categories', {
            #'description':_('The page template'),
            'classes': ('default',),
            'fields': ('categories',)
        }),
        ('Images', {
            #'description':_('The page template'),
            'classes': ('default',),
            'fields': ('images',)
        }),
    )

    class Media:
        css = {
            "all": ("admin/css/page.css",)
        }
        js = ("admin/js/page.js",)

admin.site.register(Event, EventAdmin)





# Category translation

class CategoryTranslationInline(TranslationInline):
    model = CategoryTranslation
    extra = 0 if settings.PREFIX_DEFAULT_LOCALE else 1
    prepopulated_fields = {'slug': ('title',)}
    template = 'admin/cmsbase/cms_translation_inline.html'



# Category

class CategoryAdmin(MPTTModelAdmin):

    list_display = ["title", "identifier", "published", 'order_id', 'languages']
    inlines = (CategoryTranslationInline, )
    mptt_indent_field = 'title'
    mptt_level_indent = 20

    def title(self, obj):
        translation = obj.translated() #PageTranslation.objects.filter(parent=obj, language_code=settings.DEFAULT_LANGUAGE)
        if translation:
            return translation.title
        else:
            return _('No translation available for default language')

    def languages(self, obj):
        ts=[]
        for t in obj.get_translations():
            ts.append(u'<img src="/static/admin/img/flags/%s.png" alt="" rel="tooltip" data-title="%s">' % (t.language_code, t.__unicode__()))
        return ' '.join(ts)

    languages.allow_tags = True
    languages.short_description = 'Translations'

    # Override the list display from PublishingWorkflowAdmin
    def get_list_display(self, request, obj=None):
        if not settings.PREFIX_DEFAULT_LOCALE:
            return ["title", "identifier", "published", 'order_id']
        else:
            return ["title", "identifier", "published", 'order_id', 'languages']

    fieldsets = (

        
        ('Settings', {
            #'description':_('The page template'),
            'classes': ('default',),
            'fields': ('published', 'parent', 'order_id', 'identifier', )
        }),

    )

    class Media:
        css = {
            "all": ("admin/css/page.css",)
        }
        js = ("admin/js/page.js",)

admin.site.register(Category, CategoryAdmin)

class EventLocationAdmin(admin.ModelAdmin):
    model = EventLocation
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(EventLocation, EventLocationAdmin)

class EventSpeakerAdmin(admin.ModelAdmin):
    model = EventSpeaker
    list_display= ['name', 'job_title']

    def name(self, obj):
        return obj
    #prepopulated_fields = {'slug': ('title',)}

admin.site.register(EventSpeaker, EventSpeakerAdmin)

class EventRSVPAdmin(admin.ModelAdmin):
    model = EventSpeaker
    list_display= ['name', 'email', 'organisation', 'event', 'get_status']
    list_filter = ['status']

    def name(self, obj):
        return obj.name

    def get_status(self, obj):
        if obj.status == 'ATTENDING':
            return '<i class="icon-ok"></i>'
        else:
            return '<i class="icon-minus-sign"></i>'
    get_status.allow_tags = True
    get_status.short_description = 'Attending'
    #prepopulated_fields = {'slug': ('title',)}

admin.site.register(RSVP, EventRSVPAdmin)




