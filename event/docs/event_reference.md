Event reference
===============

Method
------

`Event.speakers()`: Returns a list of speakers
 
`Event.attendees()`: Returns a list of attendees
 
`Event.links()`: Returns a list of links