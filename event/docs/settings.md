Settings
========

## Event settings

`EVENT_TEMPLATES`: [type: tuple] A tuple of event page templates

`EVENT_CONTACT_EMAIL`: The email to contact when there are no places left to RSVP.

## Archice settings

`ARCHIVE_SHOW_EMPTY`: [type: Boolean] Decide whether ot not to display years and months with no events in the archive

`ARCHIVE_SHOW_EVENTS`: [type: Boolean] When creating the archive list, decide whether ot not to display the events within each month

`ARCHIVE_SHOW_COUNT`: [type: Boolean] Show event count next to month