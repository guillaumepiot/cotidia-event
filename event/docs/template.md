Templates
=========

event_tags.py
-------------

### Inclusion tags

`event_nav` renders a navigation list of all the published events

	{% event_nav %}

`event_categories` renders a list of all published categories

	{% event_categories %}
	
`event_archive` renders a chronological archive. 

	{% event_archive %}
	
Options:

A few options are available to dictate the content of the archive:
	
	# When creating the archive list, decide whether ot not to display years and months with no articles in them
	ARCHIVE_SHOW_EMPTY = getattr(settings, 'ARCHIVE_SHOW_EMPTY', False)
	# When creating the archive list, decide whether ot not to display the articles within each month
	ARCHIVE_SHOW_EVENTS = getattr(settings, 'ARCHIVE_SHOW_EVENTS', False)
	# Show article count next to month
	ARCHIVE_SHOW_COUNT = getattr(settings, 'ARCHIVE_SHOW_COUNT', False)

You can also pass those options on a tag basis by passing extra arguments to the template tag:

	{% event_archive 'show_empty' 'show_events' 'show_count' %}
	
For example, an archive that shows the articles and the article count per year & month will be:
	
	{% blog_archive 0 1 1 %}
	
`get_upcoming_events` populate the template context with a list of upcoming events.

	{% get_upcoming_events 'limit' as upcoming_events %}
	
Eg:
	
	{% get_upcoming_events '1' as upcoming_events %}
	{% for event in upcoming_events %}
		{{event}}
	{% endfor %}

`{% get_all_speakers as speakers %}`


rsvp_tags.py
-------------

### RSVP inclusion tags

`{% rsvp_js %}`

Renders the form AJAX js.

`{% rsvp_html event_id %}`

Renders the HTML form for a specific event.
NOTE: The event id must be the one of the original event instance (not the published one):

Eg: 
	
	{% get_upcoming_events '1' as events %}
	{% for event in events %}
		{% rsvp_html event.published_from.id %}
	{% endfor %}
