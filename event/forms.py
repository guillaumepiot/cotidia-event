from django import forms
from django.utils.translation import ugettext_lazy as _

from event.models import Event, RSVP

class RSVPForm(forms.ModelForm):
	status = forms.ChoiceField(label=_('Will you attend?'), widget=forms.RadioSelect(), choices=(('ATTENDING','Yes'),('DECLINED','No')))
	event = forms.ModelChoiceField(queryset=Event.objects.get_published_original(), widget=forms.HiddenInput, empty_label=None)
	name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':_('Full name')}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':_('Email')}))
	organisation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':_('Organisation')}), required=False)
	class Meta:
		model = RSVP
		exclude = ['IP_address',]

	def clean_email(self):
		email = self.cleaned_data['email']
		event = self.cleaned_data['event']
		# Look for duplicates
		rsvps = RSVP.objects.filter(event=event, email=email).count()
		if rsvps > 0:
			raise forms.ValidationError(_('This email has already been registered.'))
		else:
			return email