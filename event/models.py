from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.core.urlresolvers import reverse

from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from multilingual_model.models import MultilingualModel, MultilingualTranslation
from cmsbase.models import BasePage, PublishTranslation, BasePageManager


from event import settings as event_settings


# Subclass the PageTranslation model to create the event translation

class EventTranslation(MultilingualTranslation, PublishTranslation):
    parent = models.ForeignKey('Event', related_name='translations')
    title = models.CharField(_('Event title'), max_length=100)
    slug = models.SlugField(max_length=100)
    content = models.TextField(blank=True)

    #Meta data
    meta_title = models.CharField(max_length=100, blank=True)
    meta_description = models.TextField(blank=True)

    class Meta:
        unique_together = ('parent', 'language_code')

        if len(settings.LANGUAGES) > 1:
            verbose_name=_('Translation')
            verbose_name_plural=_('Translations')
        else:
            verbose_name=_('Content')
            verbose_name_plural=_('Content')

    def __unicode__(self):
        return dict(settings.LANGUAGES).get(self.language_code)


# class EventImage(models.Model):

#     def call_naming(self, instance=None):
#         from cmsbase.widgets import get_media_upload_to

#         # return get_media_upload_to(self.page.slug, 'pages')
#         location = "event/%s/%s"%(self.parent.start_date.year, self.parent.start_date.month)
#         return get_media_upload_to(location, instance)

#     parent = models.ForeignKey('Event')
#     image = models.ImageField(upload_to=call_naming, max_length=100)
#     # Ordering
#     order_id = models.IntegerField(blank=True, null=True)

#     class Meta:
#         ordering = ('order_id',)
#         verbose_name = _('Image')
#         verbose_name_plural = _('Images')

#     def delete(self, *args, **kwargs):
#         from sorl.thumbnail import get_thumbnail
#         storage, path = self.image.storage, self.image.path
#         super(EventImage, self).delete(*args, **kwargs)
#         # Physically delete the file
#         storage.delete(path)

class EventLocation(models.Model):

    title = models.CharField(_('Location title'), max_length=100)
    slug = models.SlugField(max_length=100)

    #Address
    first_line = models.CharField(_('First line'), max_length=100, blank=True)
    second_line = models.CharField(_('Second line'), max_length=100, blank=True)
    city = models.CharField(_('City'),max_length=100, blank=True)
    county = models.CharField(_('County'),max_length=100, blank=True)
    postcode = models.CharField(_('Postcode / Zip'), max_length=255, blank=True)
    country = models.CharField(_('Country'),max_length=255, blank=True) 
    map_link = models.CharField(_('Map link'), max_length=255, blank=True)
    
    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('title',)
        verbose_name = _('Location')
        verbose_name_plural = _('Locations')

    def __unicode__(self):
        return u'%s' % self.title

    @property
    def address_line(self):
        lines = [self.first_line, self.second_line, self.city, self.postcode, self.county, self.country]
        output = []
        for line in lines:
            if line:
                output.append(line)

        return ", ".join(output)

class EventSpeakerRelation(models.Model):
    event = models.ForeignKey('Event')
    speaker = models.ForeignKey('EventSpeaker')

class EventSpeaker(models.Model):

    first_name = models.CharField(_('First name'), max_length=100)
    last_name = models.CharField(_('Last name'), max_length=100, blank=True)

    job_title = models.CharField(_('Job title'), max_length=100, blank=True)
    description = models.TextField(blank=True)

    twitter = models.URLField(_('Twitter URL'), max_length=250, blank=True)
    facebook = models.URLField(_('Facebook URL'), max_length=250, blank=True)
    linkedin = models.URLField(_('LinkedIn URL'), max_length=250, blank=True)
    googleplus = models.URLField(_('Google+ URL'), max_length=250, blank=True)

    photo = models.ImageField(upload_to='speakers',  max_length=100, blank=True, null=True)
    

    # Ordering
    order_id = models.IntegerField(blank=True, null=True)

    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('order_id','first_name',)
        verbose_name = _('Speaker')
        verbose_name_plural = _('Speakers')

    def __unicode__(self):
        if self.last_name:
            return u'%s %s' % (self.first_name, self.last_name)
        else:
            return u'%s' % (self.first_name)


class EventAttendee(models.Model):

    parent = models.ForeignKey('Event')

    first_name = models.CharField(_('First name'), max_length=100)
    last_name = models.CharField(_('Last name'), max_length=100, blank=True)

    job_title = models.CharField(_('Job title'), max_length=100, blank=True)

    url = models.URLField(_('Link to (URL)'), max_length=250, blank=True)

    # Ordering
    order_id = models.IntegerField(blank=True, null=True)

    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('order_id','first_name',)
        verbose_name = _('Attendee')
        verbose_name_plural = _('Attendees')

    def __unicode__(self):
        if self.last_name:
            return u'%s %s' % (self.first_name, self.last_name)
        else:
            return u'%s' % (self.first_name)

class EventLink(models.Model):

    parent = models.ForeignKey('Event')

    link_name = models.CharField(_('Link to (name)'), max_length=250, blank=True, help_text=_('Eg: Click here for event info'))
    url = models.URLField(_('Link to (URL)'), max_length=250, blank=True, help_text=_('Eg: http://myevent.com/info'))

    # Ordering
    order_id = models.IntegerField(blank=True, null=True)

    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('order_id','link_name',)
        verbose_name = _('Link')
        verbose_name_plural = _('Links')

    def __unicode__(self):
        return u'%s' % (self.link_name)

class EventManager(BasePageManager):
    pass

# Subclass the Page model to create the event model

class Event(BasePage):
    # Extra fields
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)

    location = models.ForeignKey('EventLocation', blank=True, null=True)
    categories = TreeManyToManyField('Category', blank=True)
    capacity = models.IntegerField(_('Capacity'), blank=True, null=True, help_text=_('The number of people who can RSVP'))
    # Manager
    objects = EventManager()

    class Meta:
        verbose_name=_('Event')
        verbose_name_plural=_('Events')
        ordering = ['-start_date']

    class CMSMeta:
    
        # A tuple of templates paths and names
        templates = event_settings.EVENT_TEMPLATES
        
        # Indicate which Translation class to use for content
        translation_class = EventTranslation
        #image_class =  EventImage

        # Provide the url name to create a url for that model
        model_url_name = 'event:event'

    def __unicode__(self):
        return '%s (%s.%s.%s)' % (self.unicode_wrapper('title', default='Unnamed'),  self.start_date.day, self.start_date.month, self.start_date.year)

    def speakers(self):
        # Published version
        if self.published_from:
            _speakers = EventSpeakerRelation.objects.filter(event=self.published_from)
        # Original version
        else:
            _speakers = EventSpeakerRelation.objects.filter(event=self)

        _speakers = [speaker.speaker for speaker in _speakers]
        return _speakers

    def attendees(self):
        # Published version
        if self.published_from:
            _attendees = EventAttendee.objects.filter(parent=self.published_from)
        # Original version
        else:
            _attendees = EventAttendee.objects.filter(parent=self)
        return _attendees

    def links(self):
        # Published version
        if self.published_from:
            _links = EventLink.objects.filter(parent=self.published_from)
        # Original version
        else:
            _links = EventLink.objects.filter(parent=self)
        return _links

    def rsvp_count(self):
        count = RSVP.objects.filter(event=self, status='ATTENDING').count()
        return count

    def rsvp_remaining(self):
        if self.capacity:
            remaining = self.capacity - self.rsvp_count()
            if remaining < 0:
                remaining = 0
            return remaining
        else:
            return 0
        
# Event categories

class CategoryTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Category', related_name='translations')
    title = models.CharField(_('Category title'), max_length=100)
    slug = models.SlugField(max_length=100)

    class Meta:
        unique_together = ('parent', 'language_code')

        if len(settings.LANGUAGES) > 1:
            verbose_name=_('Translation')
            verbose_name_plural=_('Translations')
        else:
            verbose_name=_('Content')
            verbose_name_plural=_('Content')

    def __unicode__(self):
        return dict(settings.LANGUAGES).get(self.language_code)

    

class Category(MPTTModel, MultilingualModel):
    #MPTT parent
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    identifier = models.SlugField(max_length=100)
    published = models.BooleanField(_('Active'))
    order_id = models.IntegerField()

    class Meta:
        verbose_name=_('Category')
        verbose_name_plural=_('Categories')

    class MPTTMeta:
        order_insertion_by = ['order_id']

    class CMSMeta:
        translation_class = CategoryTranslation

    def __unicode__(self):
        return self.unicode_wrapper('title', default='Unnamed')

    def get_translations(self):
        return self.CMSMeta.translation_class.objects.filter(parent=self)

    def translated(self):
        from django.utils.translation import get_language

        try:
            translation = self.CMSMeta.translation_class.objects.get(language_code=get_language(), parent=self)
            return translation
        except:
            return self.CMSMeta.translation_class.objects.get(language_code=settings.LANGUAGE_CODE, parent=self)

    def get_absolute_url(self):
        return reverse('event:category', kwargs={'slug':self.translated().slug})


class RSVP(models.Model):
    event = models.ForeignKey(Event)
    status = models.CharField(choices=(('ATTENDING','Attending'),('DECLINED','Declined')), max_length=250)
    name = models.CharField(max_length=250)
    email = models.EmailField()
    organisation = models.CharField(max_length=250, blank=True)
    IP_address = models.CharField(max_length=250)

    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)