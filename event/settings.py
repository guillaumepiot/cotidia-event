from django.conf import settings

# The default page templates
EVENT_TEMPLATES = getattr(settings, 'EVENT_TEMPLATES', (('event/event.html', 'Default event'),))

EVENT_CONTACT_EMAIL = getattr(settings, 'EVENT_CONTACT_EMAIL', 'hello@example.com')

# When creating the archive list, decide whether ot not to display years and months with no events in them
ARCHIVE_SHOW_EMPTY = getattr(settings, 'ARCHIVE_SHOW_EMPTY', False)
# When creating the archive list, decide whether ot not to display the events within each month
ARCHIVE_SHOW_EVENTS = getattr(settings, 'ARCHIVE_SHOW_EVENTS', False)
# Show event count next to month
ARCHIVE_SHOW_COUNT = getattr(settings, 'ARCHIVE_SHOW_COUNT', False)