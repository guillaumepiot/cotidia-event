from django.contrib.sitemaps import Sitemap
from event.models import Event

class EventSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return Event.objects.get_published_live()

    def lastmod(self, obj):
        return obj.date_updated