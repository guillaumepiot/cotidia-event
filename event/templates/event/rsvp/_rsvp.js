{% load i18n %}

var options = { 
beforeSubmit: function(){
	$('.submit').attr('value','{% trans "Sending..." %}');
},
success: function(data) { 

		$('.alert').remove();
		if(data.error){
			$('.submit').attr('value','{% trans "Send" %}');
			// Clear error class first
			$('input').removeClass('error');
			$.each(data.error, function(index, value) { 
				$('[name="'+index+'"]').parents('.col-md-6:first').addClass('has-error');
			})

			//$('#form-rsvp').prepend('<div class="alert alert-danger"></div>');
			$.each(data.error, function(index, value) { 
				$('[name="'+index+'"]').parents('.col-md-6:first').next().html('<div class="alert alert-danger">'+value+'</div>');
			})
		}
		if(data.success){
			$('input[type=text]').removeClass('error')

			;
			$('#form-rsvp').fadeOut(function(){
				$('.submit').attr('value','{% trans "Send" %}');
				if(data.success == 'ATTENDING'){
					$(this).before('<div class="alert alert-success">{% trans "<strong>Thank you.</strong><br>A confirmation email will be sent to you shortly.<br>We look forward to see you there!" %}</div>');
				}
				else{
					$(this).before('<div class="alert alert-success">{% trans "Thank you for informing us that you will not be able to attend." %}</div>');

				}
				
			})
		}
        
    } 
}; 

$('#form-rsvp').ajaxForm(options);