import calendar as cal
import datetime

from django import template
register = template.Library()

from django.utils.timezone import now
from django.core.urlresolvers import reverse

from event.models import Event, Category, EventSpeaker
from event.utils import Year, Month
from event import settings as event_settings
from event.utils import get_event_list, MONTH_NAMES

@register.filter
def get_month_name(value):
    return MONTH_NAMES[int(value)-1]

@register.inclusion_tag('event/includes/_nav.html', takes_context=True)
def event_nav(context):
    request = context['request']
    events = Event.objects.get_published_live().order_by('-start_date')
    return {'events': events, 'request':request}

@register.inclusion_tag('event/includes/_categories.html', takes_context=True)
def event_categories(context):
    request = context['request']
    categories = Category.objects.filter(published=True)
    return {'categories': categories, 'request':request}

@register.inclusion_tag('event/includes/_archive.html', takes_context=True)
def event_archive(context, show_empty=event_settings.ARCHIVE_SHOW_EMPTY, show_events=event_settings.ARCHIVE_SHOW_EVENTS, show_count=event_settings.ARCHIVE_SHOW_COUNT):
    archive = {}
    year_list = []
    events = Event.objects.get_published_live().order_by('-start_date')
    request = context['request']
    if events.count() > 0:
        # Get first and last articles
        first = events[len(events)-1]
        last = events[0]
        # Define the earliest and latest years
        earliest_year = first.start_date.year
        latest_year = last.start_date.year
        # How many years?
        year_range = latest_year - earliest_year
        
        for i in range(year_range+1):
            year_list.append(Year(latest_year - i))

    return {'archive':year_list, 'request':request, 'show_empty':show_empty, 'show_events':show_events, 'show_count':show_count }


@register.assignment_tag
def get_upcoming_events(limit = 3):
    events = Event.objects.get_published_live().filter(start_date__gte=now()).order_by('start_date')[:int(limit)]
    return events

@register.assignment_tag
def get_all_speakers():
    speakers = EventSpeaker.objects.filter().order_by('first_name')
    return speakers


def delta(year, month, d):
   mm = month + d
   yy = year
   if mm > 12:
       mm, yy = mm % 12, year + mm / 12
   elif mm < 1:
       mm, yy = 12 + mm, year - 1
   return yy, mm

@register.inclusion_tag("event/includes/_calendar.html")
def event_calendar(year=False, month=False, day=False):
    today = now()

    if year and month and day:
        date = datetime.date(year=int(year), month=int(month), day=int(day))
    elif year and month:
        date = datetime.date(year=int(year), month=int(month), day=1)
        day = date.day
    else:
        date = now()
        year = date.year
        month = date.month
        day = date.day

    day_context = day

    # Set the start date
    cal.setfirstweekday(cal.SUNDAY)

    # Get the events for the current month
    event_list = get_event_list(date.year, date.month)
    
    plus_year, plus_month = delta(date.year, date.month, 1)
    minus_year, minus_month = delta(date.year, date.month, -1)
    
    next = reverse('event:archive_month', kwargs={'year':int(plus_year), 'month':int(plus_month)})
    prev = reverse('event:archive_month', kwargs={'year':int(minus_year), 'month':int(minus_month)})
    
    
    title = "%s %s" % (cal.month_name[date.month], date.year)
    
    matrix = cal.monthcalendar(date.year, date.month)
    grid = []
    for week in matrix:
        row = []
        for day in week:
            if date.year == today.year and date.month == today.month and today.day == day:
                is_today = True
            else:
                is_today = False
            if date.day == day:
                is_selected = True
            else:
                is_selected = False
            if day:
                events = False
                for _day in event_list:
                    if day == _day[0]:
                        events = _day[1]

                link = '' #Event.day_url(date.year, date.month, day, has_event)
                row.append((day, events, link, is_today, is_selected))
            else:
                row.append(None)
        grid.append(row)
    
    return {
        "title": title,
        "prev": prev,
        "next": next,
        "grid": grid,
        'year':year,
        'month':month,
        'day_context':day_context,
        'current_month':MONTH_NAMES[int(month)-1]
    }