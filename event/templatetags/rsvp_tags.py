from django import template
from django.shortcuts import get_object_or_404
register = template.Library()

from event.forms import RSVPForm
from event.models import Event
from event import settings as event_settings

##############
# RSVP email #
##############

@register.inclusion_tag('event/rsvp/_rsvp.js')
def rsvp_js():
    return {}

@register.inclusion_tag('event/rsvp/_rsvp_form.html')
def rsvp_html(event_id):
    event = get_object_or_404(Event, id=event_id)
    form = RSVPForm(initial={'event':event})
    return {'form':form, 'event':event, 'EVENT_CONTACT_EMAIL':event_settings.EVENT_CONTACT_EMAIL}