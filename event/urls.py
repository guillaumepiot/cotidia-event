from django.conf.urls import patterns, include, url

urlpatterns = patterns('event',

	# Default
	url(r'^$', 'views.upcoming', name="upcoming"),
	url(r'^past/$', 'views.past', name="past"),
	#url(r'^search/$', 'views.search', name="search"),

	# Categories
	url(r'^categories/$', 'views.categories', name="categories"),
	url(r'^category/(?P<slug>[-\w]+)/$', 'views.category', name="category"),
	
	# Archive
	url(r'^archive/(?P<year>[\d]+)/$', 'views.archive', name="archive_year"),
	url(r'^archive/(?P<year>[\d]+)/(?P<month>[\d]+)/$', 'views.archive', name="archive_month"),
	url(r'^archive/(?P<year>[\d]+)/(?P<month>[\d]+)/(?P<day>[\d]+)/$', 'views.archive', name="archive_day"),
	
	# RSVP
	url(r'^rsvp/$', 'views.rsvp_submit', name='rsvp-submit'),
	url(r'^export-rsvp/(?P<object_id>[-\d]+)?$', 'views.export_rsvp', name="event-export-rsvp"),

	# Article view
	url(r'^(?P<slug>[-\w\/]+)/$', 'views.event', name="event"),

)