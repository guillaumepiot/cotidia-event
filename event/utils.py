import datetime

from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.utils.timezone import now
from event.models import Event


MONTH_NAMES = [
	_('January'),
	_('February'),
	_('March'),
	_('April'),
	_('May'),
	_('June'),
	_('July'),
	_('August'),
	_('September'),
	_('October'),
	_('November'),
	_('December'),
]

DAY_NAMES = [
	_('Monday'),
	_('Tuesday'),
	_('Wednesday'),
	_('Thursday'),
	_('Friday'),
	_('Saturday'),
	_('Sunday'),
]

class Year(object):

	def __init__(self, year):
		self.year = year

	def months(self):
		# We only want to show months in the past, so we'll check against the current date
		current_date = now()
	 	# Create a list of Month instances for each month of the year
		month_list = []
		for i in range(12):
			# If previous year, add all months regardless
			if current_date.year > self.year:
				month_list.append(Month(self.year, 12-i))
			# Otherwise if current year or next, only show month before
			elif current_date.year == self.year:
				# Is the month inferior to the current one?
				if current_date.month > (12-i):
					month_list.append(Month(self.year, 12-i))

		return month_list

	def article_count(self):
		return Event.objects.get_published_live().filter(start_date__year=self.year).count()

	def get_absolute_url(self):
		return reverse('event:archive_year', kwargs={'year':self.year})

	def __unicode__(self):
		return u'%s' % self.year

class Month(object):

	def __init__(self, year, month):
		self.year = year
		self.month = month
		self.month_names = MONTH_NAMES

	def events(self):
		events = Event.objects.get_published_live().filter(start_date__year=self.year, start_date__month=self.month).order_by('-start_date')
		return events

	def event_count(self):
		return self.events().count()

	def month_number(self):
		return u'%s' % self.month

	def month_name(self):
		return u'%s' % self.month_names[self.month-1]

	def get_absolute_url(self):
		return reverse('event:archive_month', kwargs={'year':self.year, 'month':self.month_number()})

	def __unicode__(self):
		return self.month_name()


def last_day_of_month(year, month):
	"""return the date (as a string) of the last day of the month"""
	
	# get next month. 
	# force us into the middle of the month 
	# so we don't need to deal with edge cases. 
	nm = datetime.date(year, month, 15) + datetime.timedelta(days=31)
	
	# now force us to the beginning of next month minus one day
	eom = datetime.date(nm.year, nm.month, 1) - datetime.timedelta(days=1)
	#print eom
	return eom

# Get the events depending on a time bracket
def get_event_list(year, month):

	start_date = datetime.date(year=year, month=month, day=1)
	end_date = last_day_of_month(year, month)
	event_list=[]
	for i in range(32):
		_date = start_date + datetime.timedelta(days=i)

		# See if there's an event on that day
		events = Event.objects.get_published_live().filter(start_date__year=_date.year, start_date__month=_date.month, start_date__day=_date.day).order_by('start_date')
		if events.count() > 0:
			event_list.append((_date.day, events))
		if _date >= end_date:
			return event_list

	return event_list