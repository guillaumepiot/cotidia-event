import datetime, json, csv, codecs, StringIO

from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.utils.timezone import now
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from cmsbase.views import page_processor

from event.models import Event, EventTranslation, Category, CategoryTranslation, RSVP
from event import settings as event_settings
from event.utils import MONTH_NAMES, DAY_NAMES
from event.forms import RSVPForm

##########
# Events #
##########

@page_processor(model_class=Event, translation_class=EventTranslation)
def event(request, event, slug):

	if not event:
		template = event_settings.BLOG_TEMPLATES[0][0]
	else:
		template = event.template

	return render_to_response(template, {'event':event,}, context_instance=RequestContext(request))

def upcoming(request):
	events = Event.objects.get_published_live().filter(start_date__gte=now()).order_by('start_date')
	return render_to_response('event/list.html', {'events':events, 'type':'upcoming'}, context_instance=RequestContext(request))

def past(request):
	events = Event.objects.get_published_live().filter(start_date__lt=now()).order_by('-start_date')
	return render_to_response('event/list.html', {'events':events, 'type':'past'}, context_instance=RequestContext(request))

def categories(request):
	return render_to_response('event/categories.html', {}, context_instance=RequestContext(request))

def category(request, slug):
	_category_translation = get_object_or_404(CategoryTranslation, slug=slug, parent__published=True)
	_category = _category_translation.parent
	events = Event.objects.get_published_live().filter(published_from__categories=_category)
	return render_to_response('event/category.html', {'category':_category, 'events':events}, context_instance=RequestContext(request))

def archive(request, year, month=False, day=False):
	if month and not day:
		events = Event.objects.get_published_live().filter(start_date__year=year, start_date__month=month).order_by('-start_date')
		selected_date = datetime.date(year=int(year), month=int(month), day=1)
		#month = MONTH_NAMES[int(month)-1]
	elif month and day:
		events = Event.objects.get_published_live().filter(start_date__year=year, start_date__month=month, start_date__day=day).order_by('-start_date')
		#month = MONTH_NAMES[int(month)-1]
		selected_date = datetime.date(year=int(year), month=int(month), day=int(day))
	else:
		events = Event.objects.get_published_live().filter(start_date__year=year).order_by('-start_date')
		selected_date = datetime.date(year=int(year), month=1, day=1)

	return render_to_response('event/archive.html', {'year':year, 'month':month, 'day':day, 'events':events, 'selected_date':selected_date}, context_instance=RequestContext(request)) 

########
# RSVP #
########

def rsvp_submit(request):

	errors={}
	success=False
	initial = {}


	if request.method == "POST":
		form = RSVPForm(request.POST)
		if form.is_valid():

			rsvp = form.save()
			rsvp.IP_address = request.META['REMOTE_ADDR']
			rsvp.save()

			errors = False
			success = rsvp.status

			name = form.cleaned_data['name']
			event_name = rsvp.event.__unicode__()

			email = form.cleaned_data['email']

			if rsvp.status == 'ATTENDING':
				subject = 'RSVP confirmation for referral for %s' % event_name
				context={'name':name, 'email':email, 'event_name':event_name} 
				text_content = render_to_string('event/email/rsvp_email.txt', context)
				html_content = render_to_string('event/email/rsvp_email.html', context)

				msg = EmailMultiAlternatives(subject, text_content, settings.SENDER_EMAIL, [email])
				msg.attach_alternative(html_content, "text/html")
				msg.send()
			
		else:
			for field in form:
				if field.errors:
					errors[field.name] = field.errors
	else:
		form = RSVPForm(initial=initial)
	
	
	results = {'error':errors, 'success':success}
	return HttpResponse(
		json.dumps(results),
		content_type = 'application/json'
	)

class UnicodeWriter:
    
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
            # Redirect output to a queue
            self.queue = StringIO.StringIO()
            self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
            self.stream = f
            self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
            self.writer.writerow([s.encode("utf-8") for s in row])
            # Fetch UTF-8 output from the queue ...
            data = self.queue.getvalue()
            data = data.decode("utf-8")
            # ... and reencode it into the target encoding
            data = self.encoder.encode(data)
            # write to the target stream
            self.stream.write(data)
            # empty queue
            self.queue.truncate(0)

    def writerows(self, rows):
            for row in rows:
                    self.writerow(row)

                
#Decorator to make sure the user is logged in           
@login_required
def export_rsvp(request, object_id):
        
        #get the poll object
        event = get_object_or_404(Event, pk=object_id)
        
        #Create a file name containing the poll id
        filename = 'RSVP_%s.csv' % event.id

        
        data = RSVP.objects.filter(event=event).order_by('date_created')
        
        csv_export_io = StringIO.StringIO()
        csv_export_io_writer = UnicodeWriter(csv_export_io, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_export_io_writer.writerow([unicode('Name'),unicode('Email'),unicode('Organisation'),unicode("IP address"),unicode("Submit date")])
        
        for item in data:
                csv_export_io_writer.writerow([unicode(item.name),unicode(item.email),unicode(item.organisation),unicode(item.IP_address),unicode(item.date_created)])
                                                                                          
        response = HttpResponse(csv_export_io.getvalue(), mimetype='application/csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        return response 